package com.akfl.cases.df.controller;

import com.afkl.cases.df.Bootstrap;
import com.afkl.cases.df.dto.FareResultDTO;
import com.afkl.cases.df.model.Airport;
import com.afkl.cases.df.model.Currency;
import com.afkl.cases.df.model.Fare;
import com.afkl.cases.df.service.AirportService;
import com.afkl.cases.df.service.FareService;
import com.akfl.cases.df.travelapiadapter.Util;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by alptugd on 10/10/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Bootstrap.class)
@WebAppConfiguration
public class TestFareController {
    private MockMvc mockMvc;

    private static final String BASE_URL = "/api/fares/";
    private static final double FIRST_AMOUNT = 100d;
    private static final double SECOND_AMOUNT = 1000d;
    private static final double THIRD_AMOUNT = 10000d;
    private static final String LANG = "LANG";
    private static final String TERM = "TERM";
    private static final String FIRST_AIRPORT_NAME = "firstName";
    private static final String FIRST_AIRPORT_CODE = "firstCode";
    private static final String SECOND_AIRPORT_NAME = "secondName";
    private static final String SECOND_AIRPORT_CODE = "secondCode";
    private static final String THIRD_AIRPORT_NAME = "thirdName";
    private static final String THIRD_AIRPORT_CODE = "thirdCode";

    @MockBean
    private AirportService mockAirportService;

    @MockBean
    private FareService mockFareService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @After
    public void tearDown() {
        verifyNoMoreInteractions(mockAirportService, mockFareService);
    }

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        setExpectationsOfAirportService();
        setExpectationsOfFareService();
    }

    @Test
    public void testSearchWithCurrencyAndLang() throws Exception {
        StringBuilder builder = new StringBuilder(BASE_URL);
        builder.append(FIRST_AIRPORT_CODE).append("/").append(SECOND_AIRPORT_CODE).append("?lang=").append(
                LANG).append("&currency=").append(Currency.USD.name());
        MvcResult result = mockMvc.perform(get(builder.toString())).andExpect(
                request().asyncStarted()).andExpect(request().asyncResult(instanceOf(FareResultDTO.class))).andReturn();

        mockMvc.perform(asyncDispatch(result)).andExpect(status().isOk()).andExpect(
                content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(
                jsonPath("$.origin.name", is(FIRST_AIRPORT_NAME))).andExpect(
                jsonPath("$.origin.code", is(FIRST_AIRPORT_CODE))).andExpect(
                jsonPath("$.destination.name", is(SECOND_AIRPORT_NAME))).andExpect(
                jsonPath("$.destination.code", is(SECOND_AIRPORT_CODE))).andExpect(
                jsonPath("$.currency", is(Currency.USD.name()))).andExpect(
                jsonPath("$.amount", is(FIRST_AMOUNT)));

        verify(mockAirportService).getAirport(LANG, FIRST_AIRPORT_CODE);
        verify(mockAirportService).getAirport(LANG, SECOND_AIRPORT_CODE);
        verify(mockFareService).getFare(FIRST_AIRPORT_CODE, SECOND_AIRPORT_CODE, Currency.USD);
    }

    @Test
    public void testSearchWithLangOnly() throws Exception {
        StringBuilder builder = new StringBuilder(BASE_URL);
        builder.append(FIRST_AIRPORT_CODE).append("/").append(SECOND_AIRPORT_CODE).append("?lang=").append(
                LANG);
        MvcResult result = mockMvc.perform(get(builder.toString())).andExpect(
                request().asyncStarted()).andExpect(request().asyncResult(instanceOf(FareResultDTO.class))).andReturn();

        mockMvc.perform(asyncDispatch(result)).andExpect(status().isOk()).andExpect(
                content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(
                jsonPath("$.origin.name", is(FIRST_AIRPORT_NAME))).andExpect(
                jsonPath("$.origin.code", is(FIRST_AIRPORT_CODE))).andExpect(
                jsonPath("$.destination.name", is(SECOND_AIRPORT_NAME))).andExpect(
                jsonPath("$.destination.code", is(SECOND_AIRPORT_CODE))).andExpect(
                jsonPath("$.currency", is(Currency.EUR.name()))).andExpect(
                jsonPath("$.amount", is(SECOND_AMOUNT)));

        verify(mockAirportService).getAirport(LANG, FIRST_AIRPORT_CODE);
        verify(mockAirportService).getAirport(LANG, SECOND_AIRPORT_CODE);
        verify(mockFareService).getFare(FIRST_AIRPORT_CODE, SECOND_AIRPORT_CODE, Currency.EUR);
    }

    @Test
    public void testSearchWithCurrencyOnly() throws Exception {
        StringBuilder builder = new StringBuilder(BASE_URL);
        builder.append(THIRD_AIRPORT_CODE).append("/").append(FIRST_AIRPORT_CODE).append("?currency=").append(
                Currency.USD.name());
        MvcResult result = mockMvc.perform(get(builder.toString())).andExpect(
                request().asyncStarted()).andExpect(request().asyncResult(instanceOf(FareResultDTO.class))).andReturn();

        mockMvc.perform(asyncDispatch(result)).andExpect(status().isOk()).andExpect(
                content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(
                jsonPath("$.origin.name", is(THIRD_AIRPORT_NAME))).andExpect(
                jsonPath("$.origin.code", is(THIRD_AIRPORT_CODE))).andExpect(
                jsonPath("$.destination.name", is(FIRST_AIRPORT_NAME))).andExpect(
                jsonPath("$.destination.code", is(FIRST_AIRPORT_CODE))).andExpect(
                jsonPath("$.currency", is(Currency.USD.name()))).andExpect(
                jsonPath("$.amount", is(THIRD_AMOUNT)));

        verify(mockAirportService).getAirport("en", FIRST_AIRPORT_CODE);
        verify(mockAirportService).getAirport("en", THIRD_AIRPORT_CODE);
        verify(mockFareService).getFare(THIRD_AIRPORT_CODE, FIRST_AIRPORT_CODE, Currency.USD);
    }

    private void setExpectationsOfFareService() {
        Fare firstFare = Util.mockFare(FIRST_AIRPORT_CODE, SECOND_AIRPORT_CODE, Currency.USD, FIRST_AMOUNT);
        Fare secondFare = Util.mockFare(FIRST_AIRPORT_CODE, SECOND_AIRPORT_CODE, Currency.EUR, SECOND_AMOUNT);
        Fare thirdFare = Util.mockFare(THIRD_AIRPORT_CODE, FIRST_AIRPORT_CODE, Currency.USD, THIRD_AMOUNT);

        when(mockFareService.getFare(FIRST_AIRPORT_CODE, SECOND_AIRPORT_CODE, Currency.USD)).thenReturn(new AsyncResult<Fare>(firstFare));
        when(mockFareService.getFare(FIRST_AIRPORT_CODE, SECOND_AIRPORT_CODE, Currency.EUR)).thenReturn(new AsyncResult<Fare>(secondFare));
        when(mockFareService.getFare(THIRD_AIRPORT_CODE, FIRST_AIRPORT_CODE, Currency.USD)).thenReturn(new AsyncResult<Fare>(thirdFare));
    }

    private void setExpectationsOfAirportService() {
        Airport firstAirport = Util.mockAirport(FIRST_AIRPORT_NAME, FIRST_AIRPORT_CODE);
        Airport secondAirport = Util.mockAirport(SECOND_AIRPORT_NAME, SECOND_AIRPORT_CODE);
        Airport thirdAirport = Util.mockAirport(THIRD_AIRPORT_NAME, THIRD_AIRPORT_CODE);

        when(mockAirportService.getAirport(LANG, FIRST_AIRPORT_CODE)).thenReturn(new AsyncResult<>(firstAirport));
        when(mockAirportService.getAirport(LANG, SECOND_AIRPORT_CODE)).thenReturn(new AsyncResult<>(secondAirport));
        when(mockAirportService.getAirport("en", THIRD_AIRPORT_CODE)).thenReturn(new AsyncResult<>(thirdAirport));
        when(mockAirportService.getAirport("en", FIRST_AIRPORT_CODE)).thenReturn(new AsyncResult<>(firstAirport));
    }

}
