package com.akfl.cases.df.controller.sortinghelper;

import com.afkl.cases.df.controller.sortinghelper.ListSplitter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

import static  org.junit.Assert.*;

/**
 * Created by alptugd on 11/10/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class TestListSplitter {

    private List<Integer> list = Arrays.asList(1,2,3,4,5,6,7,8,9);

    private ListSplitter listSplitter = new ListSplitter();

    @Test
    public void testSplittingWithinBounds() {
        List<Integer> subList = listSplitter.splitList(list, 2, 3);

        assertEquals(3, subList.size());
        assertTrue(4 == subList.get(0));
        assertTrue(6 == subList.get(2));

        subList = listSplitter.splitList(list, 2, 6);
        assertEquals(3, subList.size());
        assertTrue(7 == subList.get(0));
        assertTrue(9 == subList.get(2));

        subList = listSplitter.splitList(list, 0, 4);
        assertEquals(4, subList.size());
        assertTrue(1 == subList.get(0));
        assertTrue(4 == subList.get(3));

        subList = listSplitter.splitList(list, -5, 15);
        assertEquals(9, subList.size());
        assertTrue(1 == subList.get(0));
        assertTrue(9 == subList.get(8));
    }

    @Test(expected=IllegalArgumentException.class)
    public void testIllegalSplitting() {
        listSplitter.splitList(list, 6, 2);
    }
}
