package com.akfl.cases.df.controller;

import com.afkl.cases.df.Bootstrap;
import com.afkl.cases.df.model.Airport;
import com.afkl.cases.df.service.AirportService;
import com.akfl.cases.df.travelapiadapter.Util;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by alptugd on 10/10/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Bootstrap.class)
@WebAppConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class TestAirportController {
    private MockMvc mockMvc;

    private static final String LANG = "LANG";
    private static final String TERM = "TERM";
    private static final String FIRST_AIRPORT_NAME = "firstName";
    private static final String FIRST_AIRPORT_CODE = "firstCode";
    private static final String SECOND_AIRPORT_NAME = "secondName";
    private static final String SECOND_AIRPORT_CODE = "secondCode";
    private static final String THIRD_AIRPORT_NAME = "thirdName";
    private static final String THIRD_AIRPORT_CODE = "thirdCode";

    @MockBean
    private AirportService mockService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        List<Airport> firstAirports = asList(
                Util.mockAirport(FIRST_AIRPORT_NAME, FIRST_AIRPORT_CODE),
                Util.mockAirport(SECOND_AIRPORT_NAME, SECOND_AIRPORT_CODE));

        List<Airport> secondAirports = asList(
                Util.mockAirport(THIRD_AIRPORT_NAME, THIRD_AIRPORT_CODE));

        List<Airport> thirdAirports = new ArrayList<>(firstAirports);
        thirdAirports.addAll(secondAirports);

        Mockito.when(mockService.findAirports(LANG, TERM, 1, 25)).thenReturn(firstAirports);
        Mockito.when(mockService.findAirports("en", TERM, 1, 25)).thenReturn(secondAirports);
        Mockito.when(mockService.findAirports(LANG, TERM, 1, 10)).thenReturn(thirdAirports);
    }

    @Test
    public void testSearchWithTermAndLang() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/airports?term="+TERM+"&lang="+LANG)).andExpect(
                request().asyncStarted()).andExpect(request().asyncResult(instanceOf(Collection.class))).andReturn();

        mockMvc.perform(asyncDispatch(result)).andExpect(status().isOk()).andExpect(
                content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(
                jsonPath("$", hasSize(2))).andExpect(jsonPath("$[0].name", is(FIRST_AIRPORT_NAME))).andExpect(
                jsonPath("$[1].name", is(SECOND_AIRPORT_NAME))).andExpect(
                jsonPath("$[0].code", is(FIRST_AIRPORT_CODE))).andExpect(jsonPath("$[1].code", is(SECOND_AIRPORT_CODE)));

        Mockito.verify(mockService).findAirports(LANG, TERM, 1, 25);
    }

    @Test
    public void testSearchWithTermOnly() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/airports?term="+TERM)).andExpect(
                request().asyncStarted()).andExpect(request().asyncResult(instanceOf(Collection.class))).andReturn();

        mockMvc.perform(asyncDispatch(result)).andExpect(status().isOk()).andExpect(
                content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(
                jsonPath("$", hasSize(1))).andExpect(jsonPath("$[0].name", is(THIRD_AIRPORT_NAME))).andExpect(
                jsonPath("$[0].code", is(THIRD_AIRPORT_CODE)));

        Mockito.verify(mockService).findAirports("en", TERM, 1, 25);
    }

    @Test
    public void testSearchWithPageAndSizeIncluded() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/airports?term="+TERM+"&lang="+LANG+"&page=1&size=10")).andExpect(
                request().asyncStarted()).andExpect(request().asyncResult(instanceOf(Collection.class))).andReturn();

        mockMvc.perform(asyncDispatch(result)).andExpect(status().isOk()).andExpect(
                content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(
                jsonPath("$", hasSize(3))).andExpect(
                jsonPath("$[0].name", is(FIRST_AIRPORT_NAME))).andExpect(
                jsonPath("$[1].name", is(SECOND_AIRPORT_NAME))).andExpect(
                jsonPath("$[0].code", is(FIRST_AIRPORT_CODE))).andExpect(
                jsonPath("$[1].code", is(SECOND_AIRPORT_CODE))).andExpect(
                jsonPath("$[2].name", is(THIRD_AIRPORT_NAME))).andExpect(
                jsonPath("$[2].code", is(THIRD_AIRPORT_CODE)));

        Mockito.verify(mockService).findAirports(LANG, TERM, 1, 10);
    }
}
