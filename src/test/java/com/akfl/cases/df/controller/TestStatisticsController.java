package com.akfl.cases.df.controller;

import com.afkl.cases.df.Bootstrap;
import com.afkl.cases.df.dto.StatisticsDTO;
import com.afkl.cases.df.service.StatisticsService;
import com.akfl.cases.df.travelapiadapter.Util;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by alptugd on 10/10/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Bootstrap.class)
@WebAppConfiguration
public class TestStatisticsController {
    private MockMvc mockMvc;

    @MockBean
    private StatisticsService mockService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        StatisticsDTO statisticsDTO = Util.newStatisticsDTO(1l, 2l, 3l, 4l, 5d, 6l, 7l);
        Mockito.when(mockService.getStatistics()).thenReturn(statisticsDTO);
    }

    @Test
    public void testGetStatistics() throws  Exception {
        MvcResult result = mockMvc.perform(get("/api/statistics")).andExpect(
                request().asyncStarted()).andExpect(request().asyncResult(instanceOf(StatisticsDTO.class))).andReturn();

        mockMvc.perform(asyncDispatch(result)).andExpect(status().isOk()).andExpect(
                content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(
                jsonPath("$.totalRequestCount", is(1))).andExpect(
                jsonPath("$.clientErrorResponseCount", is(2))).andExpect(
                jsonPath("$.serverErrorResponseCount", is(3))).andExpect(
                jsonPath("$.successResponseCount", is(4))).andExpect(
                jsonPath("$.avgResponseTime", is(5d))).andExpect(
                jsonPath("$.maxResponseTime", is(6))).andExpect(
                jsonPath("$.minResponseTime", is(7)));
        Mockito.verify(mockService).getStatistics();
    }
}
