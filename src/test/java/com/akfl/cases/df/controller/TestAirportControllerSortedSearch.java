package com.akfl.cases.df.controller;

import com.afkl.cases.df.Bootstrap;
import com.afkl.cases.df.dto.SimpleAirportDTO;
import com.afkl.cases.df.model.Airport;
import com.afkl.cases.df.service.AirportService;
import com.akfl.cases.df.travelapiadapter.Util;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by alptugd on 10/10/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Bootstrap.class)
@WebAppConfiguration
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class TestAirportControllerSortedSearch {
    private MockMvc mockMvc;

    private static final String TERM = "TERM";
    private static final String NO_ITEM_TERM = "NO_ITEM_TERM";
    private static final String FIRST_AIRPORT_NAME = "firstName";
    private static final String FIRST_AIRPORT_CODE = "firstCode";
    private static final String SECOND_AIRPORT_NAME = "secondName";
    private static final String SECOND_AIRPORT_CODE = "secondCode";
    private static final String THIRD_AIRPORT_NAME = "thirdName";
    private static final String THIRD_AIRPORT_CODE = "thirdCode";
    private static final String FOURTH_AIRPORT_NAME = "fouthName";
    private static final String FOURTH_AIRPORT_CODE = "fourthCode";
    private static final String FIFTH_AIRPORT_NAME = "fifthName";
    private static final String FIFTH_AIRPORT_CODE = "fifthCode";
    private static final String SIXTH_AIRPORT_NAME = "sixthName";
    private static final String SIXTH_AIRPORT_CODE = "sixthCode";

    @MockBean
    private AirportService mockService;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        List<Airport> airports = Arrays.asList(Util.mockAirport(FIRST_AIRPORT_NAME, FIRST_AIRPORT_CODE),
                Util.mockAirport(SECOND_AIRPORT_NAME, SECOND_AIRPORT_CODE),
                Util.mockAirport(THIRD_AIRPORT_NAME, THIRD_AIRPORT_CODE),
                Util.mockAirport(FOURTH_AIRPORT_NAME, FOURTH_AIRPORT_CODE),
                Util.mockAirport(FIFTH_AIRPORT_NAME, FIFTH_AIRPORT_CODE),
                Util.mockAirport(SIXTH_AIRPORT_NAME, SIXTH_AIRPORT_CODE));

        when(mockService.findAirportCountMatchingTerm("en", NO_ITEM_TERM)).thenReturn(0L);
        when(mockService.findAirportCountMatchingTerm("en", TERM)).thenReturn(6L);
        when(mockService.findAirports("en", TERM, 1, 6)).thenReturn(airports);
    }

    @Test
    public void testNoItemFound() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/airports/namesorted?term="+NO_ITEM_TERM+"&page=1&size=3")).andExpect(
                request().asyncStarted()).andExpect(request().asyncResult(instanceOf(Collection.class))).andReturn();

        mockMvc.perform(asyncDispatch(result)).andExpect(status().isOk()).andExpect(
                content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(
                jsonPath("$", hasSize(0)));

        Mockito.verify(mockService).findAirportCountMatchingTerm("en", NO_ITEM_TERM);
    }

    @Test
    public void testSortedListRetrieved() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/airports/namesorted?term="+TERM+"&page=2&size=2")).andExpect(
                request().asyncStarted()).andExpect(request().asyncResult(instanceOf(Collection.class))).andReturn();

        mockMvc.perform(asyncDispatch(result)).andExpect(status().isOk()).andExpect(
                content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(
                jsonPath("$", hasSize(2))).andExpect(jsonPath("$[0].name", is(FOURTH_AIRPORT_NAME))).andExpect(
                jsonPath("$[0].code", is(FOURTH_AIRPORT_CODE))).andExpect(
                jsonPath("$[1].name", is(SECOND_AIRPORT_NAME))).andExpect(
                jsonPath("$[1].code", is(SECOND_AIRPORT_CODE)));

        Mockito.verify(mockService).findAirportCountMatchingTerm("en", TERM);
        Mockito.verify(mockService).findAirports("en", TERM, 1, 6);
    }

    @Test
    public void testItemsBeyondAvailableRequested() throws Exception {
        MvcResult result = mockMvc.perform(get("/api/airports/namesorted?term="+TERM+"&page=8&size=2")).andExpect(
                request().asyncStarted()).andExpect(request().asyncResult(instanceOf(Collection.class))).andReturn();

        mockMvc.perform(asyncDispatch(result)).andExpect(status().isOk()).andExpect(
                content().contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(
                jsonPath("$", hasSize(0)));

        Mockito.verify(mockService).findAirportCountMatchingTerm("en", TERM);
    }
}
