package com.akfl.cases.df.controller.sortinghelper;

import com.afkl.cases.df.controller.sortinghelper.TotalItemCountChecker;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.junit.Assert.*;

/**
 * Created by alptugd on 11/10/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class TestTotalItemCountChecker {
    private TotalItemCountChecker totalItemCountChecker = new TotalItemCountChecker();

    @Test
    public void testTotalItemCountZero() {
        assertTrue(totalItemCountChecker.isTotalItemCountNotEnoughForPageAndSize(0, 1, 2));
    }

    @Test
    public void testTotalItemCountSmallerWrtPageAndSize() {
        assertTrue(totalItemCountChecker.isTotalItemCountNotEnoughForPageAndSize(10, 6, 2));
    }

    @Test
    public void testTotalItemCountGreaterWrtPageAndSize() {
        assertFalse(totalItemCountChecker.isTotalItemCountNotEnoughForPageAndSize(10, 2, 5));
    }
}
