package com.akfl.cases.df.travelapiadapter;

import com.afkl.cases.df.model.Airport;
import com.afkl.cases.df.travelapiadapter.TravelApiAirportService;
import com.afkl.cases.df.travelapiadapter.TravelApiGateway;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Collection;
import java.util.Iterator;

import static com.akfl.cases.df.travelapiadapter.TravelApiAirportServiceTestConfiguration.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * Created by alptugd on 10/10/2016.
 *
 * Unit testing travel api airport public services
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TravelApiAirportServiceTestConfiguration.class)
@DirtiesContext
public class TestTravelApiAirportService {

    @Autowired
    private TravelApiGateway mockApiGateway;

    @Autowired
    private TravelApiAirportService service;

    @After
    public void after() {
        verifyNoMoreInteractions(mockApiGateway);
    }

    @Test
    public void testFindAirports() {
        Collection<Airport> airports = service.findAirports(LANG, TERM, PAGE, SIZE);
        assertEquals(2, airports.size());

        Iterator<Airport> iter = airports.iterator();
        Airport first = iter.next();
        Airport second = iter.next();

        assertEquals(FIRST_AIRPORT_NAME, first.getName());
        assertEquals(FIRST_AIRPORT_CODE, first.getCode());
        assertEquals(SECOND_AIRPORT_NAME, second.getName());
        assertEquals(SECOND_AIRPORT_CODE, second.getCode());

        verify(mockApiGateway).findAirports(LANG, TERM, PAGE, SIZE);
    }

    @Test
    public void testGetAirport() throws Exception {
        Airport airport = service.getAirport(LANG, FIRST_AIRPORT_CODE).get();
        assertEquals(FIRST_AIRPORT_NAME, airport.getName());
        assertEquals(FIRST_AIRPORT_CODE, airport.getCode());

        verify(mockApiGateway).getAirportDetail(LANG, FIRST_AIRPORT_CODE);
    }

    @Test
    public void testFindAirportCountMatchingTerm() throws Exception {
        long count = service.findAirportCountMatchingTerm(LANG, TERM);
        assertEquals(TOTAL_ITEM_COUNT, count);

        verify(mockApiGateway).findAirportCountMatchingTerm(LANG, TERM);
    }
}
