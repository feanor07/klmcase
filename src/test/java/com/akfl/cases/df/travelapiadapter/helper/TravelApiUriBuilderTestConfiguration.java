package com.akfl.cases.df.travelapiadapter.helper;

import com.afkl.cases.df.travelapiadapter.TravelApiConfigurationProperties;
import com.afkl.cases.df.travelapiadapter.helper.TravelApiUriBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by alptugd on 10/10/2016.
 */
@Configuration
@EnableConfigurationProperties(TravelApiConfigurationProperties.class)
public class TravelApiUriBuilderTestConfiguration {

    @Autowired
    private TravelApiConfigurationProperties properties;

    @Bean
    public TravelApiUriBuilder travelApiUriBuilder() {
        return new TravelApiUriBuilder(properties);
    }
}
