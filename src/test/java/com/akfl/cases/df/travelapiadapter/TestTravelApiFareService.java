package com.akfl.cases.df.travelapiadapter;

import com.afkl.cases.df.model.Fare;
import com.afkl.cases.df.travelapiadapter.TravelApiFareService;
import com.afkl.cases.df.travelapiadapter.TravelApiGateway;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.akfl.cases.df.travelapiadapter.TravelApiFareServiceTestConfiguration.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * Created by alptugd on 10/10/2016.
 *
 * Unit testing travel api fare public services
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TravelApiFareServiceTestConfiguration.class)
public class TestTravelApiFareService {

    @Autowired
    private TravelApiGateway mockApiGateway;

    @Autowired
    private TravelApiFareService service;

    @After
    public void after() {
        verifyNoMoreInteractions(mockApiGateway);
    }

    @Test
    public void testGetFare() throws Exception {

        Fare fare = service.getFare(ORIGIN, DESTINATION, CURRENCY).get();
        assertEquals(AMOUNT, fare.getAmount(), 0.00001d);
        assertEquals(ORIGIN, fare.getOrigin());
        assertEquals(DESTINATION, fare.getDestination());
        assertEquals(CURRENCY, fare.getCurrency());

        verify(mockApiGateway).getFare(ORIGIN, DESTINATION, CURRENCY.name());
    }
}
