package com.akfl.cases.df.travelapiadapter;

import com.afkl.cases.df.model.Airport;
import com.afkl.cases.df.model.Fare;
import com.afkl.cases.df.travelapiadapter.TravelApiGatewayImpl;
import com.afkl.cases.df.travelapiadapter.helper.TravelApiUriBuilder;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestOperations;

import java.util.Collection;
import java.util.Iterator;

import static com.akfl.cases.df.travelapiadapter.TravelApiGatewayImplTestConfiguration.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.test.annotation.DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD;

/**
 * Created by alptugd on 10/10/2016.
 *
 * Unit testing travel api fare public services
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TravelApiGatewayImplTestConfiguration.class)
@DirtiesContext(classMode = AFTER_EACH_TEST_METHOD)
public class TestTravelApiGatewayImpl {
    @Autowired
    private TravelApiUriBuilder mockUriBuilder;

    @Autowired
    private RestOperations mockRestOperations;

    @Autowired
    private TravelApiGatewayImpl travelApiGateway;

    @After
    public void after() {
        verifyNoMoreInteractions(mockUriBuilder, mockRestOperations);
    }

    @Test
    public void testFindAirportCountMatchingTerm() {
        assertEquals(TOTAL_ELEMENT_COUNT, travelApiGateway.findAirportCountMatchingTerm(LANG, TERM));
        verify(mockUriBuilder).airPortSearchServicePath(LANG, TERM, 1, 1);
        verify(mockRestOperations).exchange(AIRPORT_SEARCH_SERVICE_PATH, HttpMethod.GET, null, AIRPORT_SEARCH_TYPE);
    }

    @Test
    public void testSearchAirports() {
        Collection<Airport> airports = travelApiGateway.findAirports(LANG, TERM, PAGE, SIZE);

        assertEquals(2, airports.size());

        Iterator<Airport> iter = airports.iterator();
        Airport first = iter.next();
        Airport second = iter.next();

        assertEquals(FIRST_AIRPORT_NAME, first.getName());
        assertEquals(FIRST_AIRPORT_CODE, first.getCode());
        assertEquals(SECOND_AIRPORT_NAME, second.getName());
        assertEquals(SECOND_AIRPORT_CODE, second.getCode());

        verify(mockUriBuilder).airPortSearchServicePath(LANG, TERM, PAGE, SIZE);
        verify(mockRestOperations).exchange(AIRPORT_SEARCH_SERVICE_PATH, HttpMethod.GET, null, AIRPORT_SEARCH_TYPE);
    }

    @Test
    public void testGetAirportDetail() {
        Airport airport = travelApiGateway.getAirportDetail(LANG, FIRST_AIRPORT_CODE);
        assertEquals(FIRST_AIRPORT_NAME, airport.getName());
        assertEquals(FIRST_AIRPORT_CODE, airport.getCode());

        verify(mockUriBuilder).airPortDetailServicePath(LANG, FIRST_AIRPORT_CODE);
        verify(mockRestOperations).exchange(AIRPORT_DETAIL_SERVICE_PATH, HttpMethod.GET, null, AIRPORT_DETAIL_TYPE);
    }

    @Test
    public void testGetFare() {
        Fare fare = travelApiGateway.getFare(ORIGIN, DESTINATION, CURRENCY.name());
        assertEquals(AMOUNT, fare.getAmount(), 0.00001d);
        assertEquals(ORIGIN, fare.getOrigin());
        assertEquals(DESTINATION, fare.getDestination());
        assertEquals(CURRENCY, fare.getCurrency());

        verify(mockUriBuilder).faresServicePath(ORIGIN, DESTINATION, CURRENCY.name());
        verify(mockRestOperations).exchange(FARE_SERVICE_PATH, HttpMethod.GET, null, FARE_TYPE);
    }
 }
