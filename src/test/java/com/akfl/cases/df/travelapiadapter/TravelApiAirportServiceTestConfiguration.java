package com.akfl.cases.df.travelapiadapter;

import com.afkl.cases.df.model.Airport;
import com.afkl.cases.df.travelapiadapter.TravelApiAirportService;
import com.afkl.cases.df.travelapiadapter.TravelApiGateway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

import static java.util.Arrays.asList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by alptugd on 10/10/2016.
 */
@Configuration
public class TravelApiAirportServiceTestConfiguration {

    public static final String LANG = "lang";
    public static final String TERM = "term";
    public static final String FIRST_AIRPORT_NAME = "firstName";
    public static final String FIRST_AIRPORT_CODE = "firstCode";
    public static final String SECOND_AIRPORT_NAME = "secondName";
    public static final String SECOND_AIRPORT_CODE = "secondCode";
    public static int PAGE = 1;
    public static int SIZE = 10;
    public static long TOTAL_ITEM_COUNT = 100L;

    @Bean
    public TravelApiAirportService service() {
        return new TravelApiAirportService(apiGateway());
    }

    @Bean
    public TravelApiGateway apiGateway() {
        TravelApiGateway apiGateway = mock(TravelApiGateway.class);

        List<Airport> airports = asList(
                Util.mockAirport(FIRST_AIRPORT_NAME, FIRST_AIRPORT_CODE),
                Util.mockAirport(SECOND_AIRPORT_NAME, SECOND_AIRPORT_CODE));
        when(apiGateway.findAirports(LANG, TERM, PAGE, SIZE)).thenReturn(airports);

        Airport airport = Util.mockAirport(FIRST_AIRPORT_NAME, FIRST_AIRPORT_CODE);
        when(apiGateway.getAirportDetail(LANG, FIRST_AIRPORT_CODE)).thenReturn(airport);

        when(apiGateway.findAirportCountMatchingTerm(LANG, TERM)).thenReturn(TOTAL_ITEM_COUNT);

        return apiGateway;
    }
}
