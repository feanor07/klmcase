package com.akfl.cases.df.travelapiadapter;

import com.afkl.cases.df.model.Airport;
import com.afkl.cases.df.model.Currency;
import com.afkl.cases.df.model.Fare;
import com.afkl.cases.df.travelapiadapter.TravelApiGatewayImpl;
import com.afkl.cases.df.travelapiadapter.helper.TravelApiUriBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestOperations;

import java.util.Collection;

import static java.util.Arrays.asList;
import static org.mockito.Mockito.*;

/**
 * Created by alptugd on 10/10/2016.
 */
@Configuration
public class TravelApiGatewayImplTestConfiguration {

    public static final int PAGE = 1;
    public static final int SIZE = 10;
    public static final long TOTAL_ELEMENT_COUNT = 100;
    public static final double AMOUNT = 1234d;
    public static final String LANG = "lang";
    public static final String TERM = "term";
    public static final String ORIGIN = "ORIGIN";
    public static final Currency CURRENCY = Currency.USD;
    public static final String DESTINATION = "CURRENCY";
    public static final String AIRPORT_DETAIL_SERVICE_PATH = "AIRPORT_DETAIL_SERVICE_PATH";
    public static final String AIRPORT_SEARCH_SERVICE_PATH = "AIRPORT_SEARCH_SERVICE_PATH";
    public static final String FARE_SERVICE_PATH = "FARE_SERVICE_PATH";
    public static final String FIRST_AIRPORT_NAME = "firstName";
    public static final String FIRST_AIRPORT_CODE = "firstCode";
    public static final String SECOND_AIRPORT_NAME = "secondName";
    public static final String SECOND_AIRPORT_CODE = "secondCode";
    public static final ParameterizedTypeReference<PagedResources<Airport>> AIRPORT_SEARCH_TYPE = new ParameterizedTypeReference<PagedResources<Airport>>() {
    };
    public static final ParameterizedTypeReference<Airport> AIRPORT_DETAIL_TYPE = new ParameterizedTypeReference<Airport>() {
    };
    public static final ParameterizedTypeReference<Fare> FARE_TYPE = new ParameterizedTypeReference<Fare>() {
    };

    @Bean
    public TravelApiGatewayImpl travelApiGateway() {
        return new TravelApiGatewayImpl(restOperations(), uriBuilder());
    }

    @Bean
    public TravelApiUriBuilder uriBuilder() {
        TravelApiUriBuilder uriBuilder = mock(TravelApiUriBuilder.class);

        when(uriBuilder.airPortDetailServicePath(LANG, FIRST_AIRPORT_CODE)).thenReturn(AIRPORT_DETAIL_SERVICE_PATH);
        when(uriBuilder.airPortSearchServicePath(LANG, TERM, PAGE, SIZE)).thenReturn(AIRPORT_SEARCH_SERVICE_PATH);
        when(uriBuilder.airPortSearchServicePath(LANG, TERM, 1, 1)).thenReturn(AIRPORT_SEARCH_SERVICE_PATH);
        when(uriBuilder.faresServicePath(ORIGIN, DESTINATION, CURRENCY.name())).thenReturn(FARE_SERVICE_PATH);

        return uriBuilder;
    }

    @Bean
    public RestOperations restOperations() {
        RestOperations restOperations = mock(RestOperations.class);

        Collection<Airport> airports = asList(Util.mockAirport(FIRST_AIRPORT_NAME, FIRST_AIRPORT_CODE),
                Util.mockAirport(SECOND_AIRPORT_NAME, SECOND_AIRPORT_CODE));
        PagedResources<Airport> responseResult = mock(PagedResources.class);
        when(responseResult.getContent()).thenReturn(airports);
        when(responseResult.getMetadata()).thenReturn(new PagedResources.PageMetadata(1,1,TOTAL_ELEMENT_COUNT));
        addExchangeExpectation(restOperations, AIRPORT_SEARCH_SERVICE_PATH, AIRPORT_SEARCH_TYPE, responseResult);

        Airport airport = Util.mockAirport(FIRST_AIRPORT_NAME, FIRST_AIRPORT_CODE);
        addExchangeExpectation(restOperations, AIRPORT_DETAIL_SERVICE_PATH, AIRPORT_DETAIL_TYPE, airport);

        Fare fare = Util.mockFare(ORIGIN, DESTINATION, CURRENCY, AMOUNT);
        addExchangeExpectation(restOperations, FARE_SERVICE_PATH, FARE_TYPE, fare);

        return restOperations;
    }

    private <T> void addExchangeExpectation(RestOperations restOperations, String url,
                                            ParameterizedTypeReference<T> typeReference, T result) {
        ResponseEntity<Object> responseEntity = responseEntity(result);
        when(restOperations.exchange(eq(url), eq(HttpMethod.GET), eq(null), any(typeReference.getClass()))).
                thenReturn(responseEntity);
    }

    private <T> ResponseEntity<T> responseEntity(T result) {
        ResponseEntity<T> responseEntity = mock(ResponseEntity.class);

        when(responseEntity.getBody()).thenReturn(result);

        return responseEntity;
    }
}
