package com.akfl.cases.df.travelapiadapter;

import com.afkl.cases.df.model.Currency;
import com.afkl.cases.df.model.Fare;
import com.afkl.cases.df.travelapiadapter.TravelApiFareService;
import com.afkl.cases.df.travelapiadapter.TravelApiGateway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by alptugd on 10/10/2016.
 */
@Configuration
public class TravelApiFareServiceTestConfiguration {

    public static final String ORIGIN = "ORIGIN";
    public static final String DESTINATION = "DESTINATION";
    public static final Currency CURRENCY = Currency.USD;
    public static final double AMOUNT = 1234d;

    @Bean
    public TravelApiFareService service() {
        return new TravelApiFareService(apiGateway());
    }

    @Bean
    public TravelApiGateway apiGateway() {
        TravelApiGateway apiGateway = mock(TravelApiGateway.class);

        Fare fare = Util.mockFare(ORIGIN, DESTINATION, CURRENCY, AMOUNT);
        when(apiGateway.getFare(ORIGIN, DESTINATION, CURRENCY.name())).thenReturn(fare);

        return apiGateway;
    }


}
