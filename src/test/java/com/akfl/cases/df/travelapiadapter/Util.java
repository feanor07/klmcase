package com.akfl.cases.df.travelapiadapter;

import com.afkl.cases.df.dto.StatisticsDTO;
import com.afkl.cases.df.model.Airport;
import com.afkl.cases.df.model.Currency;
import com.afkl.cases.df.model.Fare;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by alptugd on 10/10/2016.
 */
public class Util {
    public static Airport mockAirport(String name, String code) {
        Airport airport = mock(Airport.class);
        when(airport.getCode()).thenReturn(code);
        when(airport.getName()).thenReturn(name);

        return airport;
    }

    public static Fare mockFare(String origin, String destination, Currency currency, double amount) {
        Fare fare = mock(Fare.class);
        when(fare.getOrigin()).thenReturn(origin);
        when(fare.getDestination()).thenReturn(destination);
        when(fare.getCurrency()).thenReturn(currency);
        when(fare.getAmount()).thenReturn(amount);

        return fare;
    }

    public static StatisticsDTO newStatisticsDTO(long totalRequestCount, long clientErrorResponseCount, long serverErrorResponseCount,
                                                  long successResponseCount, double avgResponseTime, long maxResponseTime, long minResponseTime)
    {
        return new StatisticsDTO(totalRequestCount, clientErrorResponseCount, serverErrorResponseCount,
                successResponseCount, avgResponseTime,maxResponseTime, minResponseTime);
    }
}
