package com.akfl.cases.df.travelapiadapter.helper;

import com.afkl.cases.df.travelapiadapter.helper.TravelApiUriBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;

/**
 * Created by alptugd on 10/10/2016.
 *
 * Unittest for testing uris to reach mock service.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TravelApiUriBuilderTestConfiguration.class,
        initializers = ConfigFileApplicationContextInitializer.class)
public class TestTravelApiUriBuilder {

    @Autowired
    private TravelApiUriBuilder uriBuilder;

    @Test
    public void testUriPaths() {
        assertEquals("http://localhost:8080/oauth/token", uriBuilder.authenticationServicePath());
        assertEquals("http://localhost:8080/airports/dilek?lang=alptug", uriBuilder.airPortDetailServicePath("alptug", "dilek"));
        assertEquals("http://localhost:8080/airports?term=dilek&lang=alptug&page=1&size=10", uriBuilder.airPortSearchServicePath("alptug", "dilek", 1, 10));
        assertEquals("http://localhost:8080/fares/mr./alptug?currency=dilek", uriBuilder.faresServicePath("mr.", "alptug", "dilek"));
    }
}
