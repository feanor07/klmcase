package com.akfl.cases.df.common;

import com.afkl.cases.df.common.MdcAwareCallable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.MDC;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by alptugd on 10/10/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class TestMdcAwareCallable {

    @Test
    public void testMdcPassingToCallable() throws Exception {
        final String key = "KEY";
        final  String value = "VALUE";

        MdcAwareCallable<String> callable = new MdcAwareCallable<String>() {
            @Override
            public String callWithParentMDC() throws Exception {
                return MDC.get(key);
            }
        };

        MDC.put(key, value);
        AsyncResult<String> result = new AsyncResult<>(callable.call());
        Assert.assertEquals(value, result.get());
    }
}
