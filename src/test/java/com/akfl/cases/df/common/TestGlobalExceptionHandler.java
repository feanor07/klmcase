package com.akfl.cases.df.common;

import com.afkl.cases.df.Bootstrap;
import com.afkl.cases.df.common.GlobalExceptionHandler;
import com.afkl.cases.df.controller.AirportController;
import com.afkl.cases.df.service.AirportService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.support.StaticApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import java.util.Collection;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by alptugd on 11/10/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Bootstrap.class)
@ControllerAdvice
public class TestGlobalExceptionHandler {

    private MockMvc mockMvc;

    private static final String LANG = "LANG";
    private static final String TERM = "TERM";

    @MockBean
    private AirportService mockService;

    @Autowired
    private AirportController airportController;

    @Before
    public void setUp() {
        GlobalExceptionHandler exceptionHandler = new GlobalExceptionHandler();

        mockMvc = MockMvcBuilders.standaloneSetup(airportController).setControllerAdvice(
                exceptionHandler).build();

        Mockito.when(mockService.findAirports(LANG, TERM, 1, 25)).thenThrow(
                new IllegalArgumentException("Bad arguments"));
        Mockito.when(mockService.findAirports(LANG, TERM, 2, 25)).thenThrow(
                new HttpServerErrorException(HttpStatus.BAD_GATEWAY));
        Mockito.when(mockService.findAirports(LANG, TERM, 3, 25)).thenThrow(
                new HttpServerErrorException(HttpStatus.GATEWAY_TIMEOUT));
        Mockito.when(mockService.findAirports(LANG, TERM, 4, 25)).thenThrow(
                new RuntimeException("RuntimeException"));
        Mockito.when(mockService.findAirports(LANG, TERM, 5, 25)).thenThrow(
                new IllegalStateException());
    }

    @Test
    public void testGenericExceptionHandling() throws Exception{
        MvcResult result = mockMvc.perform(get("/api/airports?term="+TERM+"&lang="+LANG+"&page=4")).andExpect(
                request().asyncStarted()).andReturn();

        mockMvc.perform(asyncDispatch(result)).andExpect(status().isServiceUnavailable());

        Mockito.verify(mockService).findAirports(LANG, TERM, 4, 25);

        result = mockMvc.perform(get("/api/airports?term="+TERM+"&lang="+LANG+"&page=5")).andExpect(
                request().asyncStarted()).andReturn();

        mockMvc.perform(asyncDispatch(result)).andExpect(status().isServiceUnavailable());

        Mockito.verify(mockService).findAirports(LANG, TERM, 5, 25);
    }

    @Test
    public void test4xxExceptionHandling() throws Exception{
        MvcResult result = mockMvc.perform(get("/api/airports?term="+TERM+"&lang="+LANG)).andExpect(
                request().asyncStarted()).andReturn();

        mockMvc.perform(asyncDispatch(result)).andExpect(status().isBadRequest());

        Mockito.verify(mockService).findAirports(LANG, TERM, 1, 25);
    }

    @Test
    public void test5xxExceptionHandling() throws Exception{
        MvcResult result = mockMvc.perform(get("/api/airports?term="+TERM+"&lang="+LANG+"&page=2")).andExpect(
                request().asyncStarted()).andReturn();

        mockMvc.perform(asyncDispatch(result)).andExpect(status().isBadGateway());

        Mockito.verify(mockService).findAirports(LANG, TERM, 2, 25);

        result = mockMvc.perform(get("/api/airports?term="+TERM+"&lang="+LANG+"&page=3")).andExpect(
                request().asyncStarted()).andReturn();

        mockMvc.perform(asyncDispatch(result)).andExpect(status().isGatewayTimeout());

        Mockito.verify(mockService).findAirports(LANG, TERM, 3, 25);
    }
}