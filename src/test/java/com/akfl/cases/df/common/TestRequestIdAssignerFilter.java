package com.akfl.cases.df.common;

import com.afkl.cases.df.common.RequestIdAssignerFilter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by alptugd on 10/10/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RequestIdAssignerFilterTestConfiguration.class)
public class TestRequestIdAssignerFilter {

    @Autowired
    private RequestIdAssignerFilter filter;

    @Mock
    private HttpServletRequest mockReq;

    @Mock
    private HttpServletResponse mockResp;

    @Mock
    private FilterChain mockFilterChain;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFilter() throws Exception {
        filter.doFilter(mockReq, mockResp, mockFilterChain);
        Mockito.verify(mockFilterChain).doFilter(mockReq, mockResp);
        Assert.assertNotNull(MDC.get("requestId"));
    }
}
