package com.akfl.cases.df.common;

import com.afkl.cases.df.common.RequestIdAssignerFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import java.util.Properties;

/**
 * Created by alptugd on 10/10/2016.
 */
@Configuration
public class RequestIdAssignerFilterTestConfiguration {
    @Bean
    public PropertySourcesPlaceholderConfigurer properties() throws Exception {
        final PropertySourcesPlaceholderConfigurer pspc = new PropertySourcesPlaceholderConfigurer();
        Properties properties = new Properties();

        properties.setProperty("logging.requestId", "requestId");

        pspc.setProperties(properties);
        return pspc;
    }

    @Bean
    public RequestIdAssignerFilter filter() {
        return new RequestIdAssignerFilter();
    }
}
