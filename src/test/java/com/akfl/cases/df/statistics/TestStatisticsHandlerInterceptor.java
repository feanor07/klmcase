package com.akfl.cases.df.statistics;

import com.afkl.cases.df.statistics.StatisticsHandlerInterceptor;
import com.afkl.cases.df.statistics.StatisticsRegistry;
import org.jboss.logging.MDC;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.servlet.http.HttpServletResponse;

import static org.mockito.Mockito.*;

/**
 * Created by alptugd on 10/10/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = StatisticsHandlerInterceptorTestConfiguration.class)
public class TestStatisticsHandlerInterceptor {
    @Autowired
    private StatisticsRegistry mockRegistry;

    @Autowired
    private StatisticsHandlerInterceptor interceptor;

    private HttpServletResponse response = mock(HttpServletResponse.class);

    @Before
    public void setUp() {
        when(response.getStatus()).thenReturn(200, 500, 404);
        MDC.put("requestId", 1);
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(mockRegistry, response);
    }

    @Test
    public void testInterceptorForOK4xx5xxTypeRequests() throws Exception {
        performTestForSuccessCall();
        performTestFor5xxCall();
        performTestFor4xxCall();
    }

    private void performTestForSuccessCall() throws Exception {
        interceptor.preHandle(null, response, null);
        verify(mockRegistry).startTimer();
        interceptor.postHandle(null, response, null, null);
        verify(response).getStatus();
        verify(mockRegistry).stopTimer(StatisticsHandlerInterceptorTestConfiguration.CONTEXT1);
        verify(mockRegistry).incrementTotalRequestCount();
        verify(mockRegistry).incrementSuccessResponseCount();
    }

    private void performTestFor5xxCall() throws Exception {
        interceptor.preHandle(null, response, null);
        verify(mockRegistry, times(2)).startTimer();
        interceptor.postHandle(null, response, null, null);
        verify(response, times(2)).getStatus();
        verify(mockRegistry).stopTimer(StatisticsHandlerInterceptorTestConfiguration.CONTEXT2);
        verify(mockRegistry, times(2)).incrementTotalRequestCount();
        verify(mockRegistry).incremenServerErrorResponseCount();
    }

    private void performTestFor4xxCall() throws Exception {
        interceptor.preHandle(null, response, null);
        verify(mockRegistry, times(3)).startTimer();
        interceptor.postHandle(null, response, null, null);
        verify(response, times(3)).getStatus();
        verify(mockRegistry).stopTimer(StatisticsHandlerInterceptorTestConfiguration.CONTEXT3);
        verify(mockRegistry, times(3)).incrementTotalRequestCount();
        verify(mockRegistry).incrementClientErrorResponseCount();
    }
}
