package com.akfl.cases.df.statistics;

import com.afkl.cases.df.statistics.StatisticsConfigurationProperties;
import com.afkl.cases.df.statistics.StatisticsRegistry;
import com.codahale.metrics.Counter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Snapshot;
import com.codahale.metrics.Timer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by alptugd on 10/10/2016.
 */
@Configuration
@EnableConfigurationProperties(StatisticsConfigurationProperties.class)
public class StatisticsRegistryTestConfiguration {

    public static final Counter TOTAL_REQUEST_COUNTER = mock(Counter.class);
    public static final Counter SUCCESS_RESPONSE_COUNTER = mock(Counter.class);
    public static final Counter SERVER_ERROR_RESPONSE_COUNTER = mock(Counter.class);
    public static final Counter CLIENT_ERROR_RESPONSE_COUNTER = mock(Counter.class);
    public static final Timer TIMER = mock(Timer.class);
    public static final Timer.Context CONTEXT = mock(Timer.Context.class);
    public static final Snapshot SNAPSHOT = mock(Snapshot.class);
    public static final long TOTAL_REQUEST_COUNT = 6;
    public static final long SUCCESS_RESPONSE_COUNT = 3;
    public static final long SERVER_ERROR_RESPONSE_COUNT = 2;
    public static final long CLIENT_ERROR_RESPONSE_COUNT = 1;
    public static final long MAX = 100;
    public static final long MIN = 10;
    public static final double AVG = 55;

    @Autowired
    public StatisticsConfigurationProperties configurationProperties;

    @Bean
    public MetricRegistry metricRegistry() {
        MetricRegistry registry = mock(MetricRegistry.class);

        when(registry.counter(configurationProperties.getTotalRequestCounter())).thenReturn(TOTAL_REQUEST_COUNTER);
        when(TOTAL_REQUEST_COUNTER.getCount()).thenReturn(TOTAL_REQUEST_COUNT);
        when(registry.counter(configurationProperties.getOKResponseCounter())).thenReturn(SUCCESS_RESPONSE_COUNTER);
        when(SUCCESS_RESPONSE_COUNTER.getCount()).thenReturn(SUCCESS_RESPONSE_COUNT);
        when(registry.counter(configurationProperties.getServerErrorResponseCounter())).thenReturn(SERVER_ERROR_RESPONSE_COUNTER);
        when(SERVER_ERROR_RESPONSE_COUNTER.getCount()).thenReturn(SERVER_ERROR_RESPONSE_COUNT);
        when(registry.counter(configurationProperties.getClientErrorResponseCounter())).thenReturn(CLIENT_ERROR_RESPONSE_COUNTER);
        when(CLIENT_ERROR_RESPONSE_COUNTER.getCount()).thenReturn(CLIENT_ERROR_RESPONSE_COUNT);
        when(registry.timer(configurationProperties.getExecutionTimer())).thenReturn(TIMER);
        when(TIMER.time()).thenReturn(CONTEXT);
        when(TIMER.getSnapshot()).thenReturn(SNAPSHOT);
        when(SNAPSHOT.getMax()).thenReturn(MAX);
        when(SNAPSHOT.getMin()).thenReturn(MIN);
        when(SNAPSHOT.getMean()).thenReturn(AVG);

        return registry;
    }

    @Bean
    public StatisticsRegistry statisticsRegistry() {
        return new StatisticsRegistry(configurationProperties, metricRegistry());
    }
}
