package com.akfl.cases.df.statistics;

import com.afkl.cases.df.statistics.StatisticsHandlerInterceptor;
import com.afkl.cases.df.statistics.StatisticsRegistry;
import com.codahale.metrics.Timer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import java.util.Properties;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by alptugd on 10/10/2016.
 */
@Configuration
public class StatisticsHandlerInterceptorTestConfiguration {

    public static final Timer.Context CONTEXT1 = mock(Timer.Context.class);
    public static final Timer.Context CONTEXT2 = mock(Timer.Context.class);
    public static final Timer.Context CONTEXT3 = mock(Timer.Context.class);


    @Bean
    public PropertySourcesPlaceholderConfigurer properties() throws Exception {
        final PropertySourcesPlaceholderConfigurer pspc = new PropertySourcesPlaceholderConfigurer();
        Properties properties = new Properties();

        properties.setProperty("logging.requestId", "requestId");

        pspc.setProperties(properties);
        return pspc;
    }

    @Bean
    public StatisticsHandlerInterceptor interceptor() {
        return new StatisticsHandlerInterceptor();
    }

    @Bean
    public StatisticsRegistry statisticsRegistry() {
        StatisticsRegistry registry = mock(StatisticsRegistry.class);

        when(registry.startTimer()).thenReturn(CONTEXT1, CONTEXT2, CONTEXT3);

        return registry;
    }
}
