package com.akfl.cases.df.statistics;

import com.afkl.cases.df.statistics.StatisticsConfigurationProperties;
import com.afkl.cases.df.statistics.StatisticsRegistry;
import com.codahale.metrics.MetricRegistry;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.akfl.cases.df.statistics.StatisticsRegistryTestConfiguration.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.verify;
/**
 * Created by alptugd on 10/10/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = StatisticsRegistryTestConfiguration.class,
        initializers = ConfigFileApplicationContextInitializer.class)
public class TestStatisticsRegistry {
    @Autowired
    private StatisticsRegistry registry;
    
    @Autowired
    private MetricRegistry mockRegistry;

    @Autowired
    private StatisticsConfigurationProperties properties;

    @Test
    public void testRegistryBehavior() {
        registry.incrementSuccessResponseCount();
        assertEquals(SUCCESS_RESPONSE_COUNT, registry.getSuccessResponseCount());
        verify(SUCCESS_RESPONSE_COUNTER).getCount();
        verify(SUCCESS_RESPONSE_COUNTER).inc();

        registry.incremenServerErrorResponseCount();
        assertEquals(SERVER_ERROR_RESPONSE_COUNT, registry.getServerResponseCount());
        verify(SERVER_ERROR_RESPONSE_COUNTER).getCount();
        verify(SERVER_ERROR_RESPONSE_COUNTER).inc();

        registry.incrementClientErrorResponseCount();
        assertEquals(CLIENT_ERROR_RESPONSE_COUNT, registry.getClientErrorResponseCount());
        verify(CLIENT_ERROR_RESPONSE_COUNTER).getCount();
        verify(CLIENT_ERROR_RESPONSE_COUNTER).inc();

        registry.incrementTotalRequestCount();
        assertEquals(TOTAL_REQUEST_COUNT, registry.getTotalRequestCount());
        verify(TOTAL_REQUEST_COUNTER).getCount();
        verify(TOTAL_REQUEST_COUNTER).inc();

        assertSame(CONTEXT, registry.startTimer());
        registry.stopTimer(CONTEXT);
        assertEquals(MAX, registry.getMaxResponseTime());
        assertEquals(MIN, registry.getMinResponseTime());
        assertEquals(AVG, registry.getAvgResponseTime(), 0.00001d);
    }
}
