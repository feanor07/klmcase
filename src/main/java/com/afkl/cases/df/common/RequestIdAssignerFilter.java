package com.afkl.cases.df.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by alptugd on 09/10/2016.
 *
 * Filter to assisgn a unique id to requests; OncePerRequestFilter is the right choice since async
 * features of servlet is being leveraged.
 */
@Component
public class RequestIdAssignerFilter extends OncePerRequestFilter {
    final static Logger logger = LoggerFactory.getLogger(RequestIdAssignerFilter.class);

    @Value("${logging.requestId}")
    private String requestId;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        MDC.put(requestId, UUID.randomUUID().toString());

        logger.debug("New request is received");
        filterChain.doFilter(request, response);
    }
}
