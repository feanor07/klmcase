package com.afkl.cases.df.common;

import org.slf4j.MDC;

import java.util.Map;
import java.util.concurrent.Callable;

/**
 * Created by alptugd on 09/10/2016.
 *
 * Helper class needed for passing slf4j's MDC context to threads running Callable for printing
 * request identifier.
 */
public abstract class MdcAwareCallable<V> implements Callable<V> {

    private final Map<String, String> parentContext;

    public MdcAwareCallable() {
        this.parentContext = MDC.getCopyOfContextMap();
    }

    @Override
    public V call() throws Exception {
        Map<String, String> currentContext = MDC.getCopyOfContextMap();
        if ( null != parentContext ) {
            MDC.setContextMap(parentContext);
        }
        try {
            return callWithParentMDC();
        } finally {
            if ( null != currentContext ) {
                MDC.setContextMap(currentContext);
            }
        }
    }

    public abstract V callWithParentMDC() throws Exception;
}
