package com.afkl.cases.df.travelapiadapter;

import com.afkl.cases.df.travelapiadapter.helper.TravelApiUriBuilder;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.hateoas.hal.Jackson2HalModule;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.web.client.RestOperations;

import static java.util.Arrays.asList;

/**
 * Created by alptugd on 08/10/2016.
 *
 * This calss is responsible to create beans to make actual remote calling to remote services.
 * Configuration properties related with travel api connection to be retrieved from properties file.
 */
@Configuration
@EnableConfigurationProperties(TravelApiConfigurationProperties.class)
public class TravelApiRestCallConfigration {

    @Autowired
    private TravelApiConfigurationProperties configurationProperties;

    @Bean
    public TravelApiUriBuilder uriBuilder() {
        return new TravelApiUriBuilder(configurationProperties);
    }

    /**
     * Creation of Oauth2 aware rest template, encapsulated from rest of the application
     *
     * @return Oauth2 aware rest template
     */
    @Bean
    public RestOperations restOperations() {
        OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(newClientCredentialsResourceDetails(), new DefaultOAuth2ClientContext());
        restTemplate.setMessageConverters(asList(newHateosMessageConverter()));

        return restTemplate;
    }

    private ClientCredentialsResourceDetails newClientCredentialsResourceDetails() {
        ClientCredentialsResourceDetails resourceDetails = new ClientCredentialsResourceDetails();
        resourceDetails.setAccessTokenUri(uriBuilder().authenticationServicePath());
        resourceDetails.setClientId(configurationProperties.getOauthClientId());
        resourceDetails.setClientSecret(configurationProperties.getOauthClientSecret());
        resourceDetails.setGrantType(configurationProperties.getOauthGrantType());

        return resourceDetails;
    }

    private HttpMessageConverter<Object> newHateosMessageConverter() {
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        converter.setSupportedMediaTypes(MediaType.parseMediaTypes("application/hal+json"));
        converter.setObjectMapper(newObjectMapper());

        return converter;
    }

    private ObjectMapper newObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.registerModule(new Jackson2HalModule());

        return mapper;
    }
}
