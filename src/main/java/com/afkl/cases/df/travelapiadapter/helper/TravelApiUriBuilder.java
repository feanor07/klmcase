package com.afkl.cases.df.travelapiadapter.helper;

import com.afkl.cases.df.travelapiadapter.TravelApiConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Created by alptugd on 09/10/2016.
 *
 * URI Builder for accessing to mock travel api services. It isolates rest of the application from dealing with
 * properties files or other configurations necessary to construct urls.
 */
@EnableConfigurationProperties(TravelApiConfigurationProperties.class)
public class TravelApiUriBuilder {

    private final TravelApiConfigurationProperties properties;

    public TravelApiUriBuilder(TravelApiConfigurationProperties properties) {
        this.properties = properties;
    }

    public String authenticationServicePath() {
        return newUriComponentsBuilder().path(properties.getOauthPath()).toUriString();
    }

    public String faresServicePath(String origin, String destination, String currency) {
        return newUriComponentsBuilder().path(properties.getFarePath()).pathSegment(origin).pathSegment(
                destination).queryParam(properties.getCurrencyParamId(), currency).toUriString();
    }

    public String airPortDetailServicePath(String lang, String code) {
        return newUriComponentsBuilder().path(properties.getAirportPath()).pathSegment(code).queryParam(
                properties.getLangParamId(), lang).toUriString();
    }

    public String airPortSearchServicePath(String lang, String term, int page, int size) {
        return newUriComponentsBuilder().path(properties.getAirportPath()).queryParam(
                properties.getTermParamId(), term).queryParam(properties.getLangParamId(), lang).
                queryParam(properties.getPageParamId(), page).queryParam(properties.getSizeParamId(), size).
                toUriString();
    }

    private UriComponentsBuilder newUriComponentsBuilder() {
        return UriComponentsBuilder.fromHttpUrl(properties.getUrl());
    }
}