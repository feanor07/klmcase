package com.afkl.cases.df.travelapiadapter;

import com.afkl.cases.df.model.Airport;
import com.afkl.cases.df.model.Fare;
import com.afkl.cases.df.travelapiadapter.helper.TravelApiUriBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestOperations;

import java.util.Collection;

/**
 * Created by alptugd on 09/10/2016.
 *
 * This class isolates rest of the application from external mock remote services. It uses {{{@link TravelApiUriBuilder}}}
 * to retrieve relevent mock service uris and {{{@link RestOperations}}}, which is created within
 * {{{@link TravelApiRestCallConfigration}}}, to actually make remote call to mock services.
 */
@Component
public class TravelApiGatewayImpl implements TravelApiGateway {
    private final RestOperations restOperations;
    private final TravelApiUriBuilder uriBuilder;

    @Autowired
    public TravelApiGatewayImpl(RestOperations restOperations, TravelApiUriBuilder uriBuilder) {
        this.restOperations = restOperations;
        this.uriBuilder = uriBuilder;
    }

    /**
     * This method makes remote call to mock service to retrieve fare information between origin adn destination
     * airports.
     *
     * @param origin
     * @param destination
     * @param currency
     * @return Fare domain entity
     */
    @Override
    public Fare getFare(String origin, String destination, String currency) {
        String uri = uriBuilder.faresServicePath(origin, destination, currency);

        return makeRemoteCall(uri, new ParameterizedTypeReference<Fare>() {});
    }

    /**
     * This method makes remote call to mock service to retrieve airports matching the search term.
     *
     * @param lang
     * @param term
     * @return collection of Airport domain entity
     */
    @Override
    public Collection<Airport> findAirports(String lang, String term, int page, int size) {
        String uri = uriBuilder.airPortSearchServicePath(lang, term, page, size);

        return makeRemoteCall(uri, new ParameterizedTypeReference<PagedResources<Airport>>() {}).
                getContent();
    }

    /**
     * This method makes remote call to mock service in order retrieve number of airports matching the search term.
     * This method simply makes a call to fetch just a single entity; afterwards it uses the metadata retrieved.
     *
     * @param lang
     * @param term
     * @return number of Airports matching the term
     */
    @Override
    public long findAirportCountMatchingTerm(String lang, String term) {
        String uri = uriBuilder.airPortSearchServicePath(lang, term, 1, 1);

        return makeRemoteCall(uri, new ParameterizedTypeReference<PagedResources<Airport>>() {}).getMetadata().
                getTotalElements();
    }

    /**
     * This method makes remote call to mock service to retrieve detail information for the airport with code.
     *
     * @param lang
     * @param code
     * @return
     */
    @Override
    public Airport getAirportDetail(String lang, String code) {
        String uri = uriBuilder.airPortDetailServicePath(lang, code);

        return makeRemoteCall(uri, new ParameterizedTypeReference<Airport>() {});
    }

    /**
     * Helper method encapsulating rest operations to perform remote calling.
     *
     * @param uri
     * @param resultClass
     * @param <T>
     * @return
     */
    private <T> T makeRemoteCall(String uri, ParameterizedTypeReference<T> resultClass) {
        ResponseEntity<T> responseEntity = restOperations.exchange(uri, HttpMethod.GET, null, resultClass);
        return responseEntity.getBody();
    }
}
