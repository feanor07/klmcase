package com.afkl.cases.df.travelapiadapter;

import com.afkl.cases.df.model.Airport;
import com.afkl.cases.df.model.Fare;

import java.util.Collection;

/**
 * Created by alptugd on 09/10/2016.
 *
 * API Gateway interface to connect to remote external service.
 */
public interface TravelApiGateway {
    Fare getFare(String origin, String destination, String currency);

    Collection<Airport> findAirports(String lang, String term, int page, int size);

    long findAirportCountMatchingTerm(String lang, String term);

    Airport getAirportDetail(String lang, String code);
}
