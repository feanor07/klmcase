package com.afkl.cases.df.travelapiadapter;

import com.afkl.cases.df.model.Airport;
import com.afkl.cases.df.service.AirportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.concurrent.Future;

/**
 * Created by alptugd on 08/10/2016.
 *
 * Airport service implementation that connects to external remote service via api gateway.
 */
@Service
public class TravelApiAirportService implements AirportService{
    final static Logger logger = LoggerFactory.getLogger(TravelApiAirportService.class);

    private final TravelApiGateway travelApiGateway;

    @Autowired
    public TravelApiAirportService(TravelApiGateway travelApiGateway) {
        this.travelApiGateway = travelApiGateway;
    }

    /**
     * Finds airports from external remote service via api gateway.
     * @param lang
     * @param term
     * @return
     */
    @Override
    public Collection<Airport> findAirports(String lang, String term, int page, int size) {
        logger.debug("About to make a sync call for searching airports containing term: {}", term);
        return travelApiGateway.findAirports(lang, term, page, size);
    }

    /**
     * Finds count of airports matching the term from external remote service via api gateway
     * @param lang
     * @param term
     * @return
     */
    @Override
    public long findAirportCountMatchingTerm(String lang, String term) {
        logger.debug("About to make a sync call for retrieving count of airports containing term: {}", term);
        return travelApiGateway.findAirportCountMatchingTerm(lang, term);
    }

    /**
     * Async airport retrieval from external remote service via api gateway in order to overcome performance
     * limitations.
     * @param lang
     * @param code
     * @return
     */
    @Override
    @Async
    public Future<Airport> getAirport(String lang, String code) {
        logger.debug("About to make an async call for retrieveing details of airport with code: {}", code);
        return new AsyncResult<Airport>(travelApiGateway.getAirportDetail(lang, code));
    }
}
