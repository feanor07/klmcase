package com.afkl.cases.df.travelapiadapter;

import com.afkl.cases.df.model.Currency;
import com.afkl.cases.df.model.Fare;
import com.afkl.cases.df.service.FareService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;

/**
 * Created by alptugd on 08/10/2016.
 *
 * Fare service implementation that connects to external remote service via api gateway.
 */
@Service
public class TravelApiFareService implements FareService {
    final static Logger logger = LoggerFactory.getLogger(TravelApiFareService.class);

    private final TravelApiGateway travelApiGateway;

    @Autowired
    public TravelApiFareService(TravelApiGateway travelApiGateway) {
        this.travelApiGateway = travelApiGateway;
    }

    /**
     * Finds fare information between origin and destination airports from external remote service via api gateway.
     * @param origin
     * @param destination
     * @param currency
     * @return
     */
    @Override
    @Async
    public Future<Fare> getFare(String origin, String destination, Currency currency) {
        logger.debug("About to make an async call for retrieveing fare between: {} and {} in currency: {}",
                origin, destination, currency);
        return new AsyncResult<Fare>(travelApiGateway.getFare(origin, destination, currency.name()));
    }
}
