package com.afkl.cases.df.travelapiadapter;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by alptugd on 09/10/2016.
 *
 * Configuration proprties related with mock service connection to be retrieved from properties file.
 */
@ConfigurationProperties(prefix = "mockService")
public class TravelApiConfigurationProperties {
    private String url, oauthPath, oauthClientId, oauthClientSecret, oauthGrantType,
            airportPath, farePath, termParamId, currencyParamId, langParamId, pageParamId, sizeParamId;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getOauthPath() {
        return oauthPath;
    }

    public void setOauthPath(String oauthPath) {
        this.oauthPath = oauthPath;
    }

    public String getOauthClientId() {
        return oauthClientId;
    }

    public void setOauthClientId(String oauthClientId) {
        this.oauthClientId = oauthClientId;
    }

    public String getOauthClientSecret() {
        return oauthClientSecret;
    }

    public void setOauthClientSecret(String oauthClientSecret) {
        this.oauthClientSecret = oauthClientSecret;
    }

    public String getOauthGrantType() {
        return oauthGrantType;
    }

    public void setOauthGrantType(String oauthGrantType) {
        this.oauthGrantType = oauthGrantType;
    }

    public String getAirportPath() {
        return airportPath;
    }

    public void setAirportPath(String airportPath) {
        this.airportPath = airportPath;
    }

    public String getFarePath() {
        return farePath;
    }

    public void setFarePath(String farePath) {
        this.farePath = farePath;
    }

    public String getTermParamId() {
        return termParamId;
    }

    public void setTermParamId(String termParamId) {
        this.termParamId = termParamId;
    }

    public String getCurrencyParamId() {
        return currencyParamId;
    }

    public void setCurrencyParamId(String currencyParamId) {
        this.currencyParamId = currencyParamId;
    }

    public String getLangParamId() {
        return langParamId;
    }

    public void setLangParamId(String langParamId) {
        this.langParamId = langParamId;
    }

    public String getPageParamId() {
        return pageParamId;
    }

    public void setPageParamId(String pageParamId) {
        this.pageParamId = pageParamId;
    }

    public String getSizeParamId() {
        return sizeParamId;
    }

    public void setSizeParamId(String sizeParamId) {
        this.sizeParamId = sizeParamId;
    }
}
