package com.afkl.cases.df.service;

import com.afkl.cases.df.dto.StatisticsDTO;

/**
 * Created by alptugd on 09/10/2016.
 *
 * Service interface for statistics related functions provided.
 */
public interface StatisticsService {
    StatisticsDTO getStatistics();
}
