package com.afkl.cases.df.service;

import com.afkl.cases.df.model.Airport;

import java.util.Collection;
import java.util.concurrent.Future;

/**
 * Created by alptugd on 08/10/2016.
 *
 * Service interface for airport related functions provided.
 */
public interface AirportService {
    Collection<Airport> findAirports(String lang, String term, int page, int size);

    long findAirportCountMatchingTerm(String lang, String term);

    Future<Airport> getAirport(String lang, String code);
}
