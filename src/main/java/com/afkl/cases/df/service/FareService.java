package com.afkl.cases.df.service;

import com.afkl.cases.df.model.Currency;
import com.afkl.cases.df.model.Fare;

import java.util.concurrent.Future;

/**
 * Created by alptugd on 08/10/2016.
 *
 * Service interface for fare related functions provided.
 */
public interface FareService {
     Future<Fare> getFare(String origin, String destination, Currency currency);
}
