package com.afkl.cases.df.model;

/**
 * Created by alptugd on 08/10/2016.
 *
 * Domain entity to represent Coordinates. It is populated via service calls when constructing Airport domain
 * entity.
 */
public class Coordinates {

    private double latitude, longitude;

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
