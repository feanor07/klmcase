package com.afkl.cases.df.model;

/**
 * Created by alptugd on 08/10/2016.
 *
 * Domain entity to represent price information for origin and destination airport codes. It is populated via service
 * calls.
 */
public class Fare {
    private String origin, destination;

    private double amount;

    private Currency currency;

    public String getOrigin() {
        return origin;
    }

    public String getDestination() {
        return destination;
    }

    public double getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }
}
