package com.afkl.cases.df.model;

/**
 * Created by alptugd on 08/10/2016.
 *
 * Domain entity to represent Airport. It is populated via service calls.
 */
public class Airport {

    private String code, name, description;
    private Coordinates coordinates;
    private Airport parent;

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public Airport getParent() {
        return parent;
    }
}
