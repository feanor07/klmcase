package com.afkl.cases.df.statistics;

import com.codahale.metrics.Timer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

/**
 * Created by alptugd on 09/10/2016.
 *
 * Interceptor written to collect statistics related with application execution. An interceptor is preferable
 * to filter due to fact that controllers work in async manner; which results in wrong statistics when filter is
 * used.
 */
public class StatisticsHandlerInterceptor implements HandlerInterceptor {
    final static Logger logger = LoggerFactory.getLogger(StatisticsHandlerInterceptor.class);

    @Autowired
    private StatisticsRegistry statisticsRegistry;

    @Value("${logging.requestId}")
    private String requestId;

    /**
     * Map of requestId and Timer.Context used for calculation of requst execution time.
     */
    private final HashMap<String, Timer.Context> map;

    public StatisticsHandlerInterceptor() {
        map = new HashMap<String, Timer.Context>();
    }

    /**
     * Starts timer for request to collect execution time information. This method relies on
     * @see com.afkl.cases.df.common.RequestIdAssignerFilter to assign a unique id to each request; so that a
     * dedicated timer context is created for that specific request.
     *
     * @param request, request for which statistics to be gathered
     * @param response, future response that will be returned
     * @param handler, not being used
     * @return true by default since the intention is not to filter some requests but to collect statistics
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        logger.debug("Starting timer for request");
        saveTimerContextRelatedWithCurrentRequest(statisticsRegistry.startTimer());
        return true;
    }

    /**
     * Stops timer started at {@link #preHandle(HttpServletRequest, HttpServletResponse, Object)} for execution
     * time collection. Increment relevant counters with respect to response's status code.
     *
     * @param request, request for which statistics to be gathered
     * @param response, response containing http status code to be used for statistics collection
     * @param handler, not being used
     * @param modelAndView, not being used
     * @throws Exception
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        statisticsRegistry.stopTimer(removeTimerContextRelatedWithCurrentRequest());
        incrementRelevantCounter(response.getStatus());
        statisticsRegistry.incrementTotalRequestCount();
        logger.debug("Stopped timer for request and incremented relevnt counters");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }

    /**
     * Helper method to increment relevant statistics counter
     *
     * @param responseStatus, status to be used for finding relevant counter
     */
    private void incrementRelevantCounter(int responseStatus) {
        HttpStatus status = HttpStatus.valueOf(responseStatus);

        if (status == HttpStatus.OK) {
            statisticsRegistry.incrementSuccessResponseCount();
        } else if (status.is4xxClientError()) {
            statisticsRegistry.incrementClientErrorResponseCount();
        } else if (status.is5xxServerError()) {
            statisticsRegistry.incremenServerErrorResponseCount();
        }
    }

    /**
     * Uses MDC for finding request id in order to save context to map
     *
     * @param context, timer context to be put to map for stopping later when response is generated
     */
    private void saveTimerContextRelatedWithCurrentRequest(Timer.Context context) {
        map.put(MDC.get(requestId), context);
    }

    /**
     * Uses MDC for finding request id in order to remove context from map
     *
     * @return context, timer context removed from map due to finishing of handling of the request
     */
    private Timer.Context removeTimerContextRelatedWithCurrentRequest() {
        return map.remove(MDC.get(requestId));
    }
}
