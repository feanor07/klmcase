package com.afkl.cases.df.statistics;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by alptugd on 09/10/2016.
 *
 * Configuration proprties related with statistics gathering to be retrieved from properties file.
 */
@ConfigurationProperties(prefix = "statistics")
public class StatisticsConfigurationProperties {
    private String  totalRequestCounter, OKResponseCounter, clientErrorResponseCounter,
            serverErrorResponseCounter, executionTimer;

    public String getTotalRequestCounter() {
        return totalRequestCounter;
    }

    public void setTotalRequestCounter(String totalRequestCounter) {
        this.totalRequestCounter = totalRequestCounter;
    }

    public String getOKResponseCounter() {
        return OKResponseCounter;
    }

    public void setOKResponseCounter(String OKResponseCounter) {
        this.OKResponseCounter = OKResponseCounter;
    }

    public String getClientErrorResponseCounter() {
        return clientErrorResponseCounter;
    }

    public void setClientErrorResponseCounter(String clientErrorResponseCounter) {
        this.clientErrorResponseCounter = clientErrorResponseCounter;
    }

    public String getServerErrorResponseCounter() {
        return serverErrorResponseCounter;
    }

    public void setServerErrorResponseCounter(String serverErrorResponseCounter) {
        this.serverErrorResponseCounter = serverErrorResponseCounter;
    }

    public String getExecutionTimer() {
        return executionTimer;
    }

    public void setExecutionTimer(String executionTimer) {
        this.executionTimer = executionTimer;
    }
}
