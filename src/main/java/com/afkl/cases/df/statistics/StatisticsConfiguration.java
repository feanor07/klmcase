package com.afkl.cases.df.statistics;

import com.codahale.metrics.MetricRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by alptugd on 10/10/2016.
 */
@EnableConfigurationProperties(StatisticsConfigurationProperties.class)
@Configuration
public class StatisticsConfiguration {
    @Autowired
    private StatisticsConfigurationProperties properties;

    @Bean
    public MetricRegistry metricRegistry() {
        return new MetricRegistry();
    }
}
