package com.afkl.cases.df.statistics;

import com.afkl.cases.df.dto.StatisticsDTO;
import com.afkl.cases.df.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by alptugd on 09/10/2016.
 *
 * Default statistics service implementation.
 */
@Service
public class StatisticsServiceImpl implements StatisticsService {

    private final StatisticsRegistry statisticsRegistry;

    @Autowired
    public StatisticsServiceImpl(StatisticsRegistry statisticsRegistry) {
        this.statisticsRegistry = statisticsRegistry;
    }

    /**
     * Construct DTO to be returned to client via {{{@link StatisticsRegistry}}}
     * @return dto to represent statistics
     */
    @Override
    public StatisticsDTO getStatistics() {
        return new StatisticsDTO(statisticsRegistry.getTotalRequestCount(),
                statisticsRegistry.getClientErrorResponseCount(), statisticsRegistry.getServerResponseCount(),
                statisticsRegistry.getSuccessResponseCount(), statisticsRegistry.getAvgResponseTime(),
                statisticsRegistry.getMaxResponseTime(), statisticsRegistry.getMinResponseTime());
    }
}
