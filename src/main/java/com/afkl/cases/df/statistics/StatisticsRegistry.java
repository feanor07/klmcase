package com.afkl.cases.df.statistics;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Snapshot;
import com.codahale.metrics.Timer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by alptugd on 09/10/2016.
 *
 * Adapter class for {{{@link MetricRegistry}}}. It is the only class in application accessing external
 * metric library. It isolates metric collection from the rest of the application.
 */
@Component
public class StatisticsRegistry {

    private final MetricRegistry metricRegistry;

    private final StatisticsConfigurationProperties properties;

    @Autowired
    public StatisticsRegistry(StatisticsConfigurationProperties properties, MetricRegistry metricRegistry) {
        this.metricRegistry = metricRegistry;
        this.properties = properties;
    }

    public void incrementTotalRequestCount() {
        incrementCounter(properties.getTotalRequestCounter());
    }

    public void incrementSuccessResponseCount() {
        incrementCounter(properties.getOKResponseCounter());
    }

    public void incrementClientErrorResponseCount() {
        incrementCounter(properties.getClientErrorResponseCounter());
    }

    public void incremenServerErrorResponseCount() {
        incrementCounter(properties.getServerErrorResponseCounter());
    }

    public long getTotalRequestCount() {
        return getCounterValue(properties.getTotalRequestCounter());
    }

    public long getSuccessResponseCount() {
        return getCounterValue(properties.getOKResponseCounter());
    }

    public long getClientErrorResponseCount() {
        return getCounterValue(properties.getClientErrorResponseCounter());
    }

    public long getServerResponseCount() {
        return getCounterValue(properties.getServerErrorResponseCounter());
    }

    public Timer.Context startTimer() {
        return metricRegistry.timer(properties.getExecutionTimer()).time();
    }

    public void stopTimer(Timer.Context context) {
        context.stop();
    }

    public double getAvgResponseTime() {
        return getSnapshot().getMean();
    }

    public long getMaxResponseTime() {
        return getSnapshot().getMax();
    }

    public long getMinResponseTime() {
        return getSnapshot().getMin();
    }

    private Snapshot getSnapshot() {
        return metricRegistry.timer(properties.getExecutionTimer()).getSnapshot();
    }

    private void incrementCounter(String counterIdentifier) {
        metricRegistry.counter(counterIdentifier).inc();
    }

    private long getCounterValue(String counterIdentifier) {
        return metricRegistry.counter(counterIdentifier).getCount();
    }
}
