package com.afkl.cases.df;


import com.afkl.cases.df.statistics.StatisticsHandlerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Web configuration that is responsible to add resource handlers and register statistics collecting interceptor.
 */
@Configuration
@EnableWebMvc
public class WebConfiguration extends WebMvcConfigurerAdapter {

    @Bean
    public StatisticsHandlerInterceptor statisticsHandlerInterceptor() {
        return new StatisticsHandlerInterceptor();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/index.html").addResourceLocations("classpath:/static/index.html");
        registry.addResourceHandler("/solution.html").addResourceLocations("classpath:/static/solution.html");
        registry.addResourceHandler("/jsresources/**").addResourceLocations("classpath:/static/jsresources/");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(statisticsHandlerInterceptor());
    }

}
