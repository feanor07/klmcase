package com.afkl.cases.df.controller.sortinghelper;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

/**
 * Created by alptugd on 11/10/2016.
 *
 * This is a simple helper to control item count with respect to page and size. Total item count must be enough
 * in order to return some items back to client.
 */
@Service
public class TotalItemCountChecker {
    public boolean isTotalItemCountNotEnoughForPageAndSize(int totalItemCount, int page, int size) {
        return totalItemCount==0 || (page-1) * size >= totalItemCount;
    }
}
