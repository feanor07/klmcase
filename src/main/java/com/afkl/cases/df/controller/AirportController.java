package com.afkl.cases.df.controller;

import com.afkl.cases.df.common.MdcAwareCallable;
import com.afkl.cases.df.controller.sortinghelper.ListSplitter;
import com.afkl.cases.df.controller.sortinghelper.TotalItemCountChecker;
import com.afkl.cases.df.dto.SimpleAirportDTO;
import com.afkl.cases.df.service.AirportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by alptugd on 08/10/2016.
 *
 * Backend rest controller for accessing airport related services from user interface.
 */
@RestController
@RequestMapping("/api/airports")
public class AirportController {
    final static Logger logger = LoggerFactory.getLogger(AirportController.class);

    private final AirportService airportService;

    @Autowired
    private ListSplitter listSplitter;

    @Autowired
    private TotalItemCountChecker totalItemCountChecker;

    @Autowired
    public AirportController(AirportService airportService) {
        this.airportService = airportService;
    }

    /**
     * Controller method to find airports matching term sorted with respect to name.
     * First an initial service call is performed to retrieve count of airports matching the term; a second
     * service call is made only if needed.
     *
     * @see MdcAwareCallable is used in order to include requester id to be included during logging.
     *
     * @param lang, possible values are nl, en
     * @param term, search term to be queried for name, description or code of airports
     * @param page, page to retrieve
     * @param size, number of airports to retrieve
     * @return A collection of airports matching term
     * @see SimpleAirportDTO
     */
    @RequestMapping(value = "/namesorted", method = GET, params = {"term"})
    public Callable<Collection<SimpleAirportDTO>> findSortedAirports(@RequestParam(value = "lang", defaultValue = "en") String lang,
                                                               @RequestParam(value="term") String term,
                                                               @RequestParam(value = "page", defaultValue = "1") int page,
                                                               @RequestParam(value = "size", defaultValue = "25") int size) {
        logger.debug("Fetching airports containing term: {}", term);
        return new MdcAwareCallable<Collection<SimpleAirportDTO>>() {
            @Override
            public Collection<SimpleAirportDTO> callWithParentMDC() throws Exception {
                // TODO: Did not have time to refactor service method in order to return int type
                int totalItemCount = (int) airportService.findAirportCountMatchingTerm(lang, term);

                if (totalItemCountChecker.isTotalItemCountNotEnoughForPageAndSize(totalItemCount, page, size)) {
                    return Collections.emptyList();
                }

                List<SimpleAirportDTO> airports = new ArrayList<>(airportService.findAirports(lang, term, 1, totalItemCount).stream().map(
                        airport -> new SimpleAirportDTO(airport.getCode(), airport.getName())).collect(Collectors.toList()));

                airports.sort(Comparator.comparing(SimpleAirportDTO::getName));

                return listSplitter.splitList(airports, page, size);
            }
        };
    }


    /**
     * Controller method to find airports matching term.
     * @see MdcAwareCallable is used in order to include requester id to be included during logging.
     *
     * @param lang, possible values are nl, en
     * @param term, search term to be queried for name, description or code of airports
     * @return A collection of airports matching term
     * @see SimpleAirportDTO
     */
    @RequestMapping(method = GET, params = "term")
    public Callable<Collection<SimpleAirportDTO>> findAirports(@RequestParam(value = "lang", defaultValue = "en") String lang,
                                                               @RequestParam(value="term") String term,
                                                               @RequestParam(value = "page", defaultValue = "1") int page,
                                                               @RequestParam(value = "size", defaultValue = "25") int size) {
        logger.debug("Fetching airports containing term: {}", term);
        return new MdcAwareCallable<Collection<SimpleAirportDTO>>() {
            @Override
            public Collection<SimpleAirportDTO> callWithParentMDC() throws Exception {
                return airportService.findAirports(lang, term, page, size).stream().map(
                        airport -> new SimpleAirportDTO(airport.getCode(), airport.getName())).collect(Collectors.toList());
            }
        };
    }

    /**
     * Exception handler written, due to fact that Mock service returns 4xx when no airports matching the
     * term are found. In order to provide a smooth user interface experience it is desirable to return an
     * empty list instead of resulting in server exception in such a case.
     * @param exc, 4xx exception thrown from mock service
     * @return, empty list of dtos
     */
    @ExceptionHandler(HttpClientErrorException.class)
    public Collection<SimpleAirportDTO> handleMockService4xxTypeExceptions(Exception exc) {
        logger.debug("Airport search resulted in no matches.");
        return Collections.emptyList();
    }
}
