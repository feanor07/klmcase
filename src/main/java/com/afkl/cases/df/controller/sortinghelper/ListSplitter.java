package com.afkl.cases.df.controller.sortinghelper;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by alptugd on 11/10/2016.
 *
 * This is a simple helper to split list with respect to page and size values.
 */
@Service
public class ListSplitter {
    public <T> List<T> splitList(List<T> source, int page, int size) {
        int startIndex = 0;

        if (page > 1) {
            startIndex = (page - 1) * size;
        }

        int endIndex = startIndex + size;

        if (endIndex > source.size()) {
            endIndex = source.size();
        }

        return source.subList(startIndex, endIndex);
    }
}
