package com.afkl.cases.df.controller;

import com.afkl.cases.df.common.MdcAwareCallable;
import com.afkl.cases.df.dto.StatisticsDTO;
import com.afkl.cases.df.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by alptugd on 09/10/2016.
 *
 * Backend rest controller for accessing statistics related services from user interface.
 */
@RestController
@RequestMapping("/api/statistics")
public class StatisticsController {

    private final StatisticsService statisticsService;

    @Autowired
    public StatisticsController(StatisticsService statisticsService) {
        this.statisticsService = statisticsService;
    }

    /**
     * Controller method to return application statistics to client. MdcAwareCallable is used in order to
     * include requester id to be included during logging.
     *
     * @return Statistics gathered during execution
     * @see StatisticsDTO
     */
    @RequestMapping(method = GET)
    public Callable<StatisticsDTO> getStatistics() {
        return new MdcAwareCallable<StatisticsDTO>() {
            @Override
            public StatisticsDTO callWithParentMDC() throws Exception {
                return statisticsService.getStatistics();
            }
        };
    }
}
