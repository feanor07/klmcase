package com.afkl.cases.df.controller;

import com.afkl.cases.df.common.MdcAwareCallable;
import com.afkl.cases.df.dto.DetailedAirportDTO;
import com.afkl.cases.df.dto.FareResultDTO;
import com.afkl.cases.df.model.Airport;
import com.afkl.cases.df.model.Currency;
import com.afkl.cases.df.model.Fare;
import com.afkl.cases.df.service.AirportService;
import com.afkl.cases.df.service.FareService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 * Created by alptugd on 08/10/2016.
 *
 * Backend rest controller for accessing fare related services from user interface.
 */
@RestController
@RequestMapping("/api/fares/{origin}/{destination}")
public class FareController {
    final static Logger logger = LoggerFactory.getLogger(FareController.class);

    private final AirportService airportService;

    private final FareService fareService;

    @Autowired
    public FareController(AirportService airportService, FareService fareService) {
        this.airportService = airportService;
        this.fareService = fareService;
    }

    /**
     *
     * Controller method to find fare between origin and target airports. MdcAwareCallable is used in order to
     * include requester id to be included during logging. This method performs 3 distinct service calls in
     * parallel via using
     * @see Future due to performance constraints.
     *
     * @param origin, code for origin airport
     * @param destination, code for destination airport
     * @param lang, language preference: nl or en
     * @param currency, currency preference: USD or EURO
     * @return
     */
    @RequestMapping(method = GET)
    public Callable<FareResultDTO> calculateFare(@PathVariable("origin") String origin,
                                                 @PathVariable("destination") String destination,
                                                 @RequestParam(value = "lang", defaultValue = "en") String lang,
                                                 @RequestParam(value = "currency", defaultValue = "EUR") String currency) {
        return new MdcAwareCallable()  {
            @Override
            public Object callWithParentMDC() throws Exception {
                logger.debug("Fetching airport fare from origin: {} to destination: {} for currency: {}", origin, destination, currency);
                Future<Fare> fareFuture = fareService.getFare(origin, destination, Currency.valueOf(currency));
                Future<Airport> originFuture = airportService.getAirport(lang, origin);
                Future<Airport> destinationFuture = airportService.getAirport(lang, destination);
                Fare fare = fareFuture.get();
                DetailedAirportDTO originAirport = converToDetailedAirportDTO(originFuture.get());
                DetailedAirportDTO destinationAirport = converToDetailedAirportDTO(destinationFuture.get());
                return new FareResultDTO(originAirport, destinationAirport, fare.getAmount(), fare.getCurrency());            }

        };
    }


    /**
     * Exception handler written, due to fact that Mock service returns 4xx when no airports matching the
     * origin or destination code are found. In order to notify clients about submitting an invalid airport
     * IllegalArgumentException is thrown; which will be converted to HttpStatus of code 400. This is more
     * informative for clients; since they will receive internal server error in case this handler is not
     * present.
     *
     * @param exc, ExecutionException that is thrown due to parallel execution strategy used while calling
     *             services
     * @throws IllegalArgumentException in case a 4xx response is received from service calls performed in
     * parallel; throws caught exception otherwise.
     */
    @ExceptionHandler(java.util.concurrent.ExecutionException.class)
    public ResponseEntity handleMockService4xxTypeExceptions(Exception exc) throws Exception {
        if (exc.getCause() instanceof HttpClientErrorException) {
            logger.debug("Fare search resulted in no matches most probably due to unexisting airport code.");
            throw new IllegalArgumentException("Invalid airport code");
        } else {
            throw exc;
        }
    }

    /**
     * Some airports are missing lat, long attribues. This method handles this case while converting domain
     * entity to client DTO.
     *
     * @param airport, domain entity to be converted to DTO
     * @return dto, containing relevant information for client
     */
    private DetailedAirportDTO converToDetailedAirportDTO(Airport airport) {
        double latitude = Optional.ofNullable(airport.getCoordinates())
                .map(c -> c.getLatitude()).orElse(Double.NaN);
        double longitude = Optional.ofNullable(airport.getCoordinates())
                .map(c -> c.getLongitude()).orElse(Double.NaN);
        return new DetailedAirportDTO(airport.getCode(), airport.getName(), airport.getDescription(),
                latitude, longitude);
    }
}