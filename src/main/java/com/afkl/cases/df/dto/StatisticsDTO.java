package com.afkl.cases.df.dto;

/**
 * Created by alptugd on 09/10/2016.
 *
 * DTO to be transferred to client side for returning displaying collected statistics about application
 */
public class StatisticsDTO {
    private final long totalRequestCount, clientErrorResponseCount, serverErrorResponseCount, successResponseCount,
        minResponseTime, maxResponseTime;

    private final double avgResponseTime;

    public StatisticsDTO(long totalRequestCount, long clientErrorResponseCount, long serverErrorResponseCount,
                         long successResponseCount, double avgResponseTime, long maxResponseTime, long minResponseTime) {
        this.totalRequestCount = totalRequestCount;
        this.clientErrorResponseCount = clientErrorResponseCount;
        this.serverErrorResponseCount = serverErrorResponseCount;
        this.successResponseCount = successResponseCount;
        this.avgResponseTime = avgResponseTime;
        this.maxResponseTime = maxResponseTime;
        this.minResponseTime = minResponseTime;
    }

    public long getTotalRequestCount() {
        return totalRequestCount;
    }

    public long getClientErrorResponseCount() {
        return clientErrorResponseCount;
    }

    public long getServerErrorResponseCount() {
        return serverErrorResponseCount;
    }

    public long getSuccessResponseCount() {
        return successResponseCount;
    }

    public double getAvgResponseTime() {
        return avgResponseTime;
    }

    public long getMinResponseTime() {
        return minResponseTime;
    }

    public long getMaxResponseTime() {
        return maxResponseTime;
    }
}
