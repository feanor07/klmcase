package com.afkl.cases.df.dto;

/**
 * Created by alptugd on 08/10/2016.
 *
 * DTO to be transferred to client side for displaying airports within auto-complete while searching
 */
public class SimpleAirportDTO {
    private String code, name;

    public SimpleAirportDTO(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
