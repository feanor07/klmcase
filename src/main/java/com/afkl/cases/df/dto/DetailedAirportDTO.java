package com.afkl.cases.df.dto;

/**
 * Created by alptugd on 08/10/2016.
 *
 * DTO to be transferred to client side for returning detailed information of airport when fare is
 * queried.
 */
public class DetailedAirportDTO {
    private final String code, name, description;
    private final double latitude, longitude;

    public DetailedAirportDTO(String code, String name, String description, double latitude, double longitude) {
        this.code = code;
        this.name = name;
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }
}
