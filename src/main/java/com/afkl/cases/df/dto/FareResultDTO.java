package com.afkl.cases.df.dto;

import com.afkl.cases.df.model.Currency;

/**
 * Created by alptugd on 08/10/2016.
 *
 * DTO to be transferred to client side for returning fare result containing detailed information about aiports
 * and price information.
 */
public class FareResultDTO {
    private final double amount;
    private final Currency currency;
    private final DetailedAirportDTO origin, destination;

    public FareResultDTO(DetailedAirportDTO origin, DetailedAirportDTO destination, double amount, Currency currency) {
        this.amount = amount;
        this.currency = currency;
        this.origin = origin;
        this.destination = destination;
    }

    public double getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public DetailedAirportDTO getOrigin() {
        return origin;
    }

    public DetailedAirportDTO getDestination() {
        return destination;
    }
}
